FROM archlinux:latest as build

RUN pacman -Syu python prjtrellis prjtrellis-db gcc make git fakeroot cmake eigen boost python-setuptools --noconfirm && rm -rf /var/cache/pacman/pkg/*
RUN useradd --no-create-home --shell=/bin/false builduser
USER builduser
RUN mkdir -p /tmp/build
WORKDIR /tmp/build/

RUN git clone https://aur.archlinux.org/nextpnr-git.git
RUN cd nextpnr-git && git reset --hard edf58e0c78e8ea188004e7d05167194638bc6e56 && sed -e 's/\(_ARCHS=\)(.*)/\1(ecp5)/g' -e 's/\(-DBUILD_GUI=\)ON/\1OFF/g' -e 's/\(depends.*\)'\''qt5-base'\''/\1/g' -i PKGBUILD && makepkg -cr

RUN git clone https://aur.archlinux.org/python-cocotb-git.git
RUN cd python-cocotb-git && git reset --hard 0cd206acc17a2fb09803bfa3ab9dab835c5a7e89 && makepkg -cr

FROM archlinux:latest
RUN pacman -Syu make yosys iverilog verilator prjtrellis prjtrellis-db python-setuptools --noconfirm && rm -rf /var/cache/pacman/pkg/* && rm -rf /var/lib/pacman/sync/*
COPY --from=build /tmp/build/nextpnr-git/nextpnr-git-*.pkg.tar.zst /
COPY --from=build /tmp/build/python-cocotb-git/python-cocotb-git-*.pkg.tar.zst /
RUN pacman -U --noconfirm nextpnr-git-*.pkg.tar.zst python-cocotb-git-*.pkg.tar.zst
