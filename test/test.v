module test(
	input clk_i
);

reg something;

always @(posedge clk_i) begin
	something <= clk_i;
end

endmodule
