#!/bin/sh -xe

cd "$(dirname "$0")"

yosys -e ".*" -p "synth_ecp5 -json test.json" test.v

nextpnr-ecp5 \
	--12k \
	--package CABGA256 \
	--speed 6 \
	--freq 65 \
	--json test.json \
	--textcfg test.config \
	--lpf test.lpf

ecppack --bit test.bit --svf test.svf --input test.config
